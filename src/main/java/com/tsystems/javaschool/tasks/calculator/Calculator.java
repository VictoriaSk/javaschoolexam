package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null) return null;
        String[] makePostfix = toPostfix(statement);
        if (!isPostfixValid(makePostfix)) return null;
        else return evaluatePostfix(makePostfix);
    }

    private String[] toPostfix(String input) {
        Stack<Character> stack = new Stack<Character>();
        StringBuilder postfixStatement = new StringBuilder();
        for (int j = 0; j < input.length(); j++) {
            char ch = input.charAt(j);
            switch (ch) {
                case '+':
                case '-':
                    postfixStatement.append(' ');
                    while (!stack.isEmpty()) {
                        char opTop = stack.pop();
                        if (opTop == '(') {
                            stack.push(opTop);
                            break;
                        } else {
                            postfixStatement.append(opTop);
                            postfixStatement.append(' ');
                        }
                    }
                    stack.push(ch);
                    break;
                case '*':
                case '/':
                    postfixStatement.append(' ');
                    while (!stack.isEmpty()) {
                        char opTop = stack.pop();
                        if (opTop == '(') {
                            stack.push(opTop);
                            break;
                        } else {
                            if (opTop == '+' || opTop == '-') {
                                stack.push(opTop);
                                break;
                            } else {
                                postfixStatement.append(opTop);
                                postfixStatement.append(' ');
                            }
                        }
                    }
                    stack.push(ch);
                    break;
                case '(':
                    stack.push(ch);
                    break;
                case ')':
                    while (!stack.isEmpty()) {
                        char chOnTop = stack.pop();
                        if (chOnTop == '(')
                            break;
                        else {
                            postfixStatement.append(' ');
                            postfixStatement.append(chOnTop);
                        }
                    }
                    break;
                default:
                    postfixStatement.append(ch);
                    break;
            }
        }
        while (!stack.isEmpty()) {
            postfixStatement.append(' ');
            postfixStatement.append(stack.pop());
        }
        String postfix = postfixStatement.toString();
        String[] splitPostfix = postfix.split(" ");
        return splitPostfix;
    }

    private boolean isPostfixValid(String[] postfixToCheck) {
        if (postfixToCheck.length < 3) return false;
        for (int i = 0; i < postfixToCheck.length; i++) {
            if (postfixToCheck[i].equals("")) return false;
            else if (postfixToCheck[i].equals("+") || postfixToCheck[i].equals("-") || postfixToCheck[i].equals("*") || postfixToCheck[i].equals("/")) {
                if ((i == 0) || (i == 1)) return false;
            } else {
                try {
                    Integer.parseInt(postfixToCheck[i]);
                } catch (NumberFormatException ex) {
                    try {
                        Double.parseDouble(postfixToCheck[i]);
                    } catch (NumberFormatException e) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private String evaluatePostfix(String[] splitPostfix) {
        Stack<Double> numbersStack = new Stack<>();
        double currentResult = 0;
        String result = null;
        Double num1 = null;
        Double num2 = null;
        for (int i = 0; i < splitPostfix.length; i++) {
            String st = splitPostfix[i];
            if (st.charAt(0) >= '0' && st.charAt(0) <= '9')
                numbersStack.push(Double.parseDouble(st));
            else {
                num2 = numbersStack.pop();
                num1 = numbersStack.pop();
                switch (st) {
                    case "+":
                        currentResult = num1 + num2;
                        break;
                    case "-":
                        currentResult = num1 - num2;
                        break;
                    case "*":
                        currentResult = num1 * num2;
                        break;
                    case "/":
                        if (Double.compare(num2, 0) == 0) {
                            return null;
                        } else {
                            currentResult = num1 / num2;
                        }
                        break;
                    default:
                        currentResult = 0;
                }
                numbersStack.push(currentResult);
            }
        }
        currentResult = numbersStack.pop();
        int integerResult = (int) currentResult;
        if (currentResult == (double) integerResult) result = Integer.toString(integerResult);
        else result = Double.toString(currentResult);
        return result;
    }
}



