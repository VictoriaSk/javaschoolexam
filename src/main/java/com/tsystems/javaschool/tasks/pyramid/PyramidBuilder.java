package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.size() > Integer.MAX_VALUE / 2) throw new CannotBuildPyramidException();
        for (int i = 0; i < inputNumbers.size(); i++)
            if (inputNumbers.get(i) == null)
                throw new CannotBuildPyramidException();
        Collections.sort(inputNumbers);

        int theSize = inputNumbers.size();
        double x1 = 0.0;
        double x2 = 0.0;
        int lines = 0;
        int c = -2 * theSize;
        int discr = 1 - 4 * c;
        if (discr >= 0) {

            x1 = (-1 - Math.sqrt(discr)) / (2);
            x2 = (-1 + Math.sqrt(discr)) / (2);
        } else {
            throw new CannotBuildPyramidException();
        }
        if (x1 > 0) {
            lines = (int) x1;
            if ((double) lines != x1) throw new CannotBuildPyramidException();
        } else if (x2 > 0) {
            lines = (int) x2;
            if ((double) lines != x2) throw new CannotBuildPyramidException();
        }

        int columns = lines * 2 - 1;
        int[][] pyramidArray = new int[lines][columns];

        int count = 0; // to count inputNumbers
        int step = lines - 1; // first element in line
        for (int line = 0; line < lines; line++) {
            pyramidArray[line][step] = inputNumbers.get(count);
            count++;
            int newStep = step + 2;
            for (int symbolsInLine = 0; symbolsInLine < line; symbolsInLine++) {
                pyramidArray[line][newStep] = inputNumbers.get(count);
                count++;
                newStep = newStep + 2;
            }
            step--;
        }
        return pyramidArray;
    }


}
