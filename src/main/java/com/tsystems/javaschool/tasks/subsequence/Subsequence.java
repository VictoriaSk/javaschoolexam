package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if ((x == null) || (y == null)) throw new IllegalArgumentException();
        if (x.size() > y.size()) return false;
        List<Integer> xIndexes = new ArrayList<>();
        List<Integer> yIndexes = new ArrayList<>();
        for (int yIndex = 0; yIndex < y.size(); yIndex++) {
            for (int xIndex = 0; xIndex < x.size(); xIndex++) {
                if (y.get(yIndex).equals(x.get(xIndex))) {
                    xIndexes.add(xIndex);
                    yIndexes.add(yIndex);
                }
            }
        }
        for (int currentX = 0; currentX < xIndexes.size() - 1; currentX++)
            if (xIndexes.get(currentX + 1) < xIndexes.get(currentX)) return false;
        for (int currentY = 0; currentY < yIndexes.size() - 1; currentY++)
            if (yIndexes.get(currentY + 1) < yIndexes.get(currentY)) return false;
        return true;
    }
}
